//funcao que vai receber o evento
function recebeEvento(evento){
    let numero_wpp = evento.detail.numero_wpp;
    console.log(numero_wpp);
}
//funcao para dispar oe vento
function disparaEvento(){
    //cria um evento personalizado
    let evento = new CustomEvent('evento_teste', {
        detail:{
            numero_wpp: 4494949494
        }
    });
    //dispara o evento no document
    document.dispatchEvent(evento);
    console.log("trigger event");
}

//adicionamos o listener para a funcao recebeevento
document.addEventListener('evento_teste', recebeEvento);

//pega o botao do DOM
let button = document.getElementById('disparar');

//coloca um listener pra quando clicar chamar a funcao disparavento
button.addEventListener('click', disparaEvento);



